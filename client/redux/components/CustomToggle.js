import React, { Component } from 'react'

export default class CustomToggle extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.onClick(e);
  }

  render() {
    const {
        search,
        items,
        children,
        open,
    } = this.props
    const chevronIcon = !search
        ? (<i className={ open === true ? 'fa fa-chevron-right' : 'fa fa-chevron-down' } aria-hidden="true" />)
        : (null)
    return (
      <div>
        <a href="" onClick={this.handleClick} className="filter-widget-row-header-title">
            {chevronIcon} {children}
        </a>
        
        <div className="filter-widget-row-header-items">
            {
                search
                    ? null
                    : items.filter((item) => {
                        return item.filtered
                    }).map((item) => {
                        return item.name
                    }).join(', ')
            }
        </div>
      </div>
    )
  }
}
