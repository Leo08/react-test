import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Dropdown, MenuItem, Checkbox } from 'react-bootstrap'
import CustomToggle from './CustomToggle'
import CustomMenu from './CustomMenu'

import {
    TABLES_GROUP,
    ROWS_GROUP,
    SORTING_ASC,
    SORTING_DESC,
    changeSorting,
    changeSearchMode,
    changeSearchString,
} from '../actions'

class FilterDropdown extends Component {
  sortItems(a, b) {
    const {
        search,
        filters: {
            sorting,
        },
    } = this.props
    if (!search) {
        return 0
    }
    if (a.name < b.name) {
        return sorting === SORTING_ASC
            ? -1
            : 1
    }
    if (a.name > b.name) {
        return sorting === SORTING_ASC
            ? 1
            : -1
    }
    return 0
  }
  render() {   
    const {
        title,
        items,
        onFilterItem,
        group,
        search,
        filters: {
            sorting,
            searchMode,
            searchString,
        },
        onChangeSorting,
        onChangeSearchMode,
        onChangeSearchString,
    } = this.props
    const toggleHeader = search
        ? (null)
        : (
            <CustomToggle bsRole="toggle" items={ items } search={ search }>
                { title }
            </CustomToggle>
        )
    return (
      <div className="dropdown-menu-row">
        <Dropdown className="dropdown-custom-menu">
            { toggleHeader }
            <CustomMenu
                bsRole="menu"
                search={ search }
                sorting={ sorting }
                onChangeSorting={ onChangeSorting }
                onChangeSearchMode={ onChangeSearchMode }
                onChangeSearchString={ onChangeSearchString }
                searchMode={ searchMode }
                searchString={ searchString }
                open={ true }
            >
                { 
                    items.sort(this.sortItems.bind(this)).map((item) => {
                        const itemId = [
                            item.id,
                            item.name,
                            typeof item.columnId !== 'undefined' ? item.columnId : '',
                            typeof item.tableId !== 'undefined' ? item.tableId : '',
                        ].join('-')
                        const checkboxAddClass = item.filtered ? 'checked' : ''
                        return (
                            <MenuItem eventKey={ item.id } onClick={ e => {
                                        onFilterItem.call(
                                                this,
                                                typeof item.id === 'undefined'
                                                    ? item.name
                                                    : item.id,
                                                group !== TABLES_GROUP
                                                    ? item.tableId
                                                    : null,
                                                group === ROWS_GROUP
                                                    ? item.columnId
                                                    : null
                                            )
                                    }
                                    }>
                                <Checkbox
                                    checked={ item.filtered }
                                    className={
                                        [
                                            'filter-checkbox',
                                            checkboxAddClass,
                                        ].join(' ')
                                    }
                                >
                                    { item.name }
                                </Checkbox>
                            </MenuItem>
                        )
                    })
                }
            </CustomMenu>
        </Dropdown>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
        filters: state.filters,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onChangeSorting: (sorting) => {
            dispatch(changeSorting(sorting))
        },
        onChangeSearchMode: (searchMode) => {
            dispatch(changeSearchMode(searchMode))
        },
        onChangeSearchString: (searchString) => {
            dispatch(changeSearchString(searchString))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FilterDropdown)
