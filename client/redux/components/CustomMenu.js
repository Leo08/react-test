import React, { Component } from 'react'
import { DropdownButton, MenuItem, InputGroup, FormControl } from 'react-bootstrap'

import {
    SORTING_ASC,
    SORTING_DESC,
    SEARCH_MODE_FULL,
    SEARCH_MODE_PART,
    SEARCH_MODE_BEGIN,
} from '../actions'

const SORTING_ASC_LETTERS = 'A-Z'
const SORTING_DESC_LETTERS = 'Z-A'

const SEARCH_MODE_FULL_LETTERS = '**'
const SEARCH_MODE_PART_LETTERS = '*__'
const SEARCH_MODE_BEGIN_LETTERS = '""'

export default class CustomMenu extends React.Component {
  constructor(props, context) {
    super(props, context)
  }
 
  
  static sortingLetters(sorting) {
      return sorting === SORTING_ASC
        ? SORTING_ASC_LETTERS
        : SORTING_DESC_LETTERS
  }
  
  static searchModeLetters(searchMode) {
      switch(searchMode) {
          case SEARCH_MODE_FULL:
              return SEARCH_MODE_FULL_LETTERS
          case SEARCH_MODE_PART:
              return SEARCH_MODE_PART_LETTERS
          case SEARCH_MODE_BEGIN:
              return SEARCH_MODE_BEGIN_LETTERS
          default:
              return ''
      }
  }
  

  render() {
    const {
        children,
        search,
        sorting,
        searchMode,
        searchString,
        onChangeSorting,
        onChangeSearchMode,
        onChangeSearchString,
    } = this.props
        
    const searchingControls = search
        ? (
            <div className="search-controls">
                <InputGroup>
                    <InputGroup.Addon><i className="fa fa-search" aria-hidden="true"></i></InputGroup.Addon>
                    <FormControl
                        ref={c => { this.input = c }}
                        type="text"
                        onChange={ e => {
                            onChangeSearchString(
                                e.target.value
                            )
                        } }
                        value={ searchString }
                    />
                </InputGroup>
                <div className="search-controls-dropdowns">
                    <DropdownButton
                        title={ CustomMenu.searchModeLetters(searchMode) }
                        id="bg-vertical-dropdown-2"
                        onSelect={ onChangeSearchMode }
                    >
                        <MenuItem eventKey={ SEARCH_MODE_FULL }>{ CustomMenu.searchModeLetters(SEARCH_MODE_FULL) }</MenuItem>
                        <MenuItem eventKey={ SEARCH_MODE_PART }>{ CustomMenu.searchModeLetters(SEARCH_MODE_PART) }</MenuItem>
                        <MenuItem eventKey={ SEARCH_MODE_BEGIN }>{ CustomMenu.searchModeLetters(SEARCH_MODE_BEGIN) }</MenuItem>
                    </DropdownButton>
                    <DropdownButton
                        title={ CustomMenu.sortingLetters(sorting) }
                        id="bg-vertical-dropdown-2"
                        onSelect={ onChangeSorting }
                    >
                        <MenuItem eventKey={ SORTING_ASC }>{ CustomMenu.sortingLetters(SORTING_ASC) }</MenuItem>
                        <MenuItem eventKey={ SORTING_DESC }>{ CustomMenu.sortingLetters(SORTING_DESC) }</MenuItem>
                    </DropdownButton>
                </div>
            </div>
        )
        : (null)
    
    return (
      <div className={ !search ? 'dropdown-menu' : 'items-menu' } style={{ padding: '' }}>
        { searchingControls }
        <ul className="list-unstyled">
          {
              React.Children.toArray(children).filter((child) => {
                  if (!search || !searchString.trim()) {
                    return true
                  }
                  const item = child.props.children.props.children
                  switch (searchMode) {
                      case SEARCH_MODE_FULL:
                          return item.indexOf(searchString) === 0 && item.length === searchString.length
                      case SEARCH_MODE_PART:
                          return item.indexOf(searchString) !== -1
                      case SEARCH_MODE_BEGIN:
                          return item.indexOf(searchString) === 0
                  }
                  return false
              })
          }
        </ul>
      </div>
    );
  }
}
