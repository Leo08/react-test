import React, {Component} from 'react'
import { connect } from 'react-redux'
import Draggable from 'react-draggable'

import FilterDropdown from './FilterDropdown'

import {
    getAllTables,
    onFilterGroup,
    changeWidgetPosition,
    TABLES_GROUP,
    COLUMNS_GROUP,
    ROWS_GROUP,
    STATE_NOT_LOADING,
    STATE_READY,
} from '../actions'


require('../sass/main.scss')

const TABLES_TITLE = 'Contexts'
const COLUMNS_TITLE = 'Dimensions'
const ROWS_TITLE = 'Rows'
const WIDGET_TITLE = 'Filters'

class WidgetFilter extends Component {
  constructor(props){
    super(props)
    if (props.status === STATE_READY && props.filteredTables.length !== 0) {
        this.state = this.rebuildTables(props.filteredTables, props.status)
        return
    }
    this.state = {
        tables: [],
        columns: [],
        rows: [],
        allData: [],
        status: STATE_NOT_LOADING,
    }    
  }
  componentDidMount() {
      if (this.state.status !== STATE_READY) {
        this.props.getTables()
      }
  }
  rebuildTables(filteredTables, status) {
      const tables = filteredTables.map((item) => {
            return {
                id: item.id,
                name: item.name,
                filtered: item.filtered,
            }
        })
        const columns = filteredTables.filter((item) => {
            return item.filtered
        }).map((item) => {
            return item.cols.map((column) => {
                return {
                    id: column.id,
                    name: column.name,
                    filtered: column.filtered,
                    tableId: item.id
                }
            })
        }).reduce(function(a, b) {
            return a.concat(b)
        }, [])
        const rows = filteredTables.filter((item) => {
            return item.filtered
        }).map((item) => {
            return item.cols.filter((column) => {
                    return column.filtered
            }).map((column) => {
                return column.rows.map((row) => {
                    return {
                        name: row.name,
                        filtered: row.filtered,
                        tableId: item.id,
                        columnId: column.id,
                    }
                })
            }).reduce(function(a, b) {
                return a.concat(b)
            }, [])
        }).reduce(function(a, b) {
            return a.concat(b)
        }, [])
        return {
            allData: filteredTables,
            tables,
            columns,
            rows,
            status,
        }
  }
  componentWillReceiveProps(nextProps) {
      const { status, filteredTables } = nextProps
      
      if (status === STATE_READY && filteredTables !== this.state.allData && filteredTables.length > 0) {
        this.setState(this.rebuildTables(filteredTables, status))
        return
      }
      if (status !== this.state.status) {
          this.setState({
            status,
          })
      }
  }
  render() {
    const {
        tables,
        columns,
        rows,
    } = this.state
    const {
        onFilterTable,
        widgetPosition,
        filteredTables,
        onChangeWidgetPosition,
    } = this.props
    return (
      <Draggable
        axis="both"
        handle=".filter-widget-header"
        defaultPosition={ widgetPosition }
        position={null}
        onStop={ (e, l) => {
            onChangeWidgetPosition(l.x, l.y)
        }}
        grid={[20, 20]}>
            <div className="filter-widget">
                <div className="filter-widget-header">
                    <i className="fa fa-bars fa-rotate-90" aria-hidden="true" />
                    <span>{ WIDGET_TITLE }</span>
                    <a className="pull-right" href="javascript:void(0)"><i className="fa fa-times" aria-hidden="true" /></a>
                </div>
                <div className="filter-widget-rows">
                    <FilterDropdown title={ TABLES_TITLE } group={ TABLES_GROUP } items={ tables } onFilterItem={ onFilterTable.bind(this, TABLES_GROUP, filteredTables) } search={ false } />
                    <FilterDropdown title={ COLUMNS_TITLE } group={ COLUMNS_GROUP } items={ columns } onFilterItem={ onFilterTable.bind(this, COLUMNS_GROUP, filteredTables) } search={ false } />
                    <FilterDropdown title={ ROWS_TITLE } group={ ROWS_GROUP } items={ rows } onFilterItem={ onFilterTable.bind(this, ROWS_GROUP, filteredTables) } search={ true } />
                </div>
                <div className="filter-widget-footer" />
            </div>
      </Draggable>
    )
  }
}

const mapStateToProps = (state, ownProps) => {   
    return Object.assign(
        {},
        state.tablesResponse,
        {
            widgetPosition: state.widgetPosition,
        }
    )
}
const mapDispatchToProps = (dispatch) => {
    return {
        getTables: () => {
            dispatch(getAllTables())
        },
        onFilterTable: (group, items, id, tableId = null, columnId = null) => {
            dispatch(onFilterGroup(group, items, id, tableId, columnId))
        },
        onChangeWidgetPosition: (x, y) => {
            dispatch(changeWidgetPosition({x, y}))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WidgetFilter)
