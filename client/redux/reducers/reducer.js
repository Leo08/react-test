import {
    GET_ALL_TABLES,
    FILTER_TABLES,
    STATE_NOT_LOADING,
    SORTING_ASC,
    SEARCH_MODE_FULL,
    CHANGE_SORTING,
    CHANGE_SEARCH_MODE,
    CHANGE_SEARCH_STRING,
    CHANGE_WIDGET_POSITION,
} from '../actions'

export const initialState = {
  tablesResponse: {
      status: STATE_NOT_LOADING,
      filteredTables: [],
  },
  filters: {
      sorting: SORTING_ASC,
      searchMode: SEARCH_MODE_FULL,
      searchString: '',
  },
  widgetPosition: {
      x: 0,
      y: 0,
  },
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_TABLES:
      return Object.assign(
          {},
          state,
          {
              tablesResponse: action.info,
          }
      )
    case FILTER_TABLES:
      return Object.assign(
          {},
          state,
          {
              tablesResponse: {
                  status: state.tablesResponse.status,
                  filteredTables: action.filteredTables,
              },
          }
      )
    case CHANGE_SORTING:
      return Object.assign(
          {},
          state,
          {
              filters: {
                  sorting: action.sorting,
                  searchMode: state.filters.searchMode,
                  searchString: state.filters.searchString,
              },
          }
      )
    case CHANGE_SEARCH_MODE:
      return Object.assign(
          {},
          state,
          {
              filters: {
                  searchMode: action.searchMode,
                  sorting: state.filters.sorting,
                  searchString: state.filters.searchString,
              },
          }
      )
    case CHANGE_SEARCH_STRING:
        return Object.assign(
          {},
          state,
          {
              filters: {
                  searchMode: state.filters.searchMode,
                  sorting: state.filters.sorting,
                  searchString: action.searchString,
              },
          }
      )
    case CHANGE_WIDGET_POSITION:
        return Object.assign(
          {},
          state,
          {
              widgetPosition: action.widgetPosition,
          }
      )
    default:
      return state
  }

}

export default reducer
