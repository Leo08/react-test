const FILTER_TABLES = "FILTER_TABLES"
const CHANGE_SORTING = "CHANGE_SORTING"
const CHANGE_SEARCH_MODE = "CHANGE_SEARCH_MODE"
const CHANGE_SEARCH_STRING = "CHANGE_SEARCH_STRING"

const TABLES_GROUP = 'TABLES'
const COLUMNS_GROUP = 'COLUMNS'
const ROWS_GROUP = 'ROWS'

const SORTING_ASC = 'SORTING_ASC'
const SORTING_DESC = 'SORTING_DESC'

const SEARCH_MODE_FULL = 'SEARCH_MODE_FULL'
const SEARCH_MODE_PART = 'SEARCH_MODE_PART'
const SEARCH_MODE_BEGIN = 'SEARCH_MODE_BEGIN'

const filterTables = (filteredTables) => ({
    type: FILTER_TABLES,
    filteredTables,
})

const changeSorting = (sorting) => ({
    type: CHANGE_SORTING,
    sorting,
})

const changeSearchMode = (searchMode) => ({
    type: CHANGE_SEARCH_MODE,
    searchMode,
})

const changeSearchString = (searchString) => ({
    type: CHANGE_SEARCH_STRING,
    searchString,
})

const onFilterGroup = (group, filteredTables, id, tableId = null, columnId = null) => dispatch => {
    const newFilteredTables = filteredTables.map((item) => {
        if (group === TABLES_GROUP && item.id === id) {
            return Object.assign(
                {},
                item,
                {
                    filtered: !item.filtered,
                }
            )
        }
        if (group === COLUMNS_GROUP && item.id === tableId) {
            let tempItem = item
            tempItem.cols = tempItem.cols.map((colItem) => {
                if (id === colItem.id) {
                    return Object.assign(
                        {},
                        colItem,
                        {
                            filtered: !colItem.filtered,
                        }
                    )
                }
                return colItem
            })
            return tempItem
        }

        if (group === ROWS_GROUP && item.id === tableId) {
            let tempItem = item
            tempItem.cols = tempItem.cols.map((colItem) => {
                if (columnId === colItem.id) {
                    let tempCol = colItem
                    tempCol.rows = tempCol.rows.map((rowItem) => {
                        if (id === rowItem.name) {
                            return Object.assign(
                                {},
                                rowItem,
                                {
                                    filtered: !rowItem.filtered,
                                }
                            )
                        }
                        return rowItem
                    })
                    return tempCol
                }
                return colItem
            })
            return tempItem
        }
        return item
    })
    
    dispatch(filterTables(newFilteredTables))
}
export {
    TABLES_GROUP,
    COLUMNS_GROUP,
    ROWS_GROUP,
    SORTING_ASC,
    SORTING_DESC,
    SEARCH_MODE_FULL,
    SEARCH_MODE_PART,
    SEARCH_MODE_BEGIN,
    FILTER_TABLES,
    CHANGE_SORTING,
    CHANGE_SEARCH_MODE,
    CHANGE_SEARCH_STRING,
    onFilterGroup,
    changeSorting,
    changeSearchMode,
    changeSearchString,
}
