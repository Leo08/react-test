const CHANGE_WIDGET_POSITION = "CHANGE_WIDGET_POSITION"

const changeWidgetPosition = (widgetPosition) => ({
    type: CHANGE_WIDGET_POSITION,
    widgetPosition,
})

export {
    CHANGE_WIDGET_POSITION,
    changeWidgetPosition,
}
