import axios from "axios"

const GET_ALL_TABLES = "GET_ALL_TABLES"


const TABLES_GROUP = 'TABLES'
const COLUMNS_GROUP = 'COLUMNS'
const ROWS_GROUP = 'ROWS'

const STATE_NOT_LOADING = "STATE_NOT_LOADING";
const STATE_WAITING = "STATE_WAITING";
const STATE_READY = "STATE_READY";


const getTables = (info) => ({
    type: GET_ALL_TABLES,
    info,
})

const getAllTables = () => dispatch => {
  dispatch(
    getTables({
        status: STATE_WAITING,
    })
  )
  axios.get(`http://localhost:3000/json-data/tables.json`)
    .then((response) => {
      return response.data.tables;
    })
    .then((tables) => {
      const filteredTables = tables.map((table) => {
          return {
              id: table.id,
              name: table.name,
              filtered: false,
              cols: table.columns.map((column) => {
                  const rows = table.rows.map((item) => {
                    return {
                        name: item[column.name],
                        filtered: false,
                    }
                  })
                  return {
                      id: column.id,
                      name: column.name,
                      filtered: false,
                      rows,
                  }
              })
          }
      })
      dispatch(
          getTables({
            status: STATE_READY,
            filteredTables,
          })
      )
    })
    .catch((err) => {
        dispatch(
          getTables({
            status: STATE_NOT_LOADING,
          })
        )
      console.error.bind(err);
    })
}

export {
    STATE_NOT_LOADING,
    STATE_WAITING,
    STATE_READY,
    GET_ALL_TABLES,
    getAllTables,
}
