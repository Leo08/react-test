import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import _ from 'lodash'
import { createStore, compose, applyMiddleware } from 'redux'

import reducer, { initialState } from '../reducers/reducer'
import { loadState, saveState } from './localStorage'

export const configureStore = () => {
  const persistedState = Object.assign(
      initialState,
      loadState(),
  )
  const store = createStore(
      reducer,
      persistedState,
      compose(
        applyMiddleware(
            createLogger(),
            thunkMiddleware,
        ),
      ),
  )
  
  store.subscribe(_.throttle(() => {
    const loadedState = store.getState()
    saveState({
      tablesResponse: loadedState.tablesResponse,
      filters: loadedState.filters,
      widgetPosition: loadedState.widgetPosition,
    })
  }, 1000))

  return store
}
